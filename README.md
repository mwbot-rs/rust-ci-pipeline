# rust-ci-pipeline

GitLab CI pipeline for mwbot-rs projects. If you want to reuse this in your
own project you're encouraged to fork this, or use [legoktm's](https://gitlab.com/legoktm/rust-ci-pipeline)
version of it, which is a bit more general.
